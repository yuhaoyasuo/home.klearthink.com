import { getRelativePath } from './utils';

/* head meta tag data */
export const SITE_TITLE = 'KlearThink Design 澄思設計';
export const SITE_DESC =
  'KlearThink Design 澄思設計';
export const SITE_URL = 'www.klearthink.com';
export const FAVICON_PATH = getRelativePath('/static/favicon-new.ico?ver=2.0');
export const LARGE_ICON_PATH = getRelativePath(
  '/static/large-icon-new.png?ver=2.0',
);
export const NAV_TITLE = 'nav title';

export enum i18nNamespace {
  Home = 'home',
}

export const GA_ID = process.env.GA_ID;

export const API_ENDPOINT = '';

export const langsMap: { [key: string]: boolean } = {
  'zh-TW': true,
  en: true,
};
