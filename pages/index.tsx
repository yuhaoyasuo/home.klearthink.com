import { NextFC } from 'next';
import React from 'react'
import Home from '../components/home/Home';
import { i18nNamespace, SITE_TITLE } from '../constants';
import HomeLayout from '../layouts/Home';

const Index: NextFC = () => {
  return (
    <HomeLayout title={'KlearThink | ' + SITE_TITLE}>
      <Home id="section-home" />
    </HomeLayout>
  );
};

Index.getInitialProps = async () => ({
  namespacesRequired: [i18nNamespace.Home],
});

export default Index;
